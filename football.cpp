#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstring>

using namespace std;

enum Side { HOME, VISITORS };

const int MAX_PLAYERS = 100;

class FootballPlayer {
private:
    // TODO
    string playername;
    int playerscore;
    
public:
    FootballPlayer(const string &name);
    const string &getName() const;
    int getPointsScored() const;
    void recordScore(int points);
};
FootballPlayer::FootballPlayer(const string &name){
    playername = name;
    playerscore = 0;
    //cout<<"created player fp"<<"\t";
};
const string &FootballPlayer::getName() const{
    //cout<<"getname";
    return playername;
};
int FootballPlayer::getPointsScored() const{
    //cout<<"getpoint";
    return playerscore;
};
void FootballPlayer::recordScore(int points){
    //cout<<"record";
    if((points<7&&points>0)&&points!=4&&points!=5) //only valid scoring 1, 2, 3, 6
        playerscore+=points;
};

// TODO: FootballPlayer methods


class FootballTeam {
private:
    // TODO
    FootballPlayer **team; //list of players
    string teamname;  //team name
    int teamint; //player counter

public:
    FootballTeam(const string &name);
    ~FootballTeam(){
        for(int i = 0; i<teamint; i++){
            delete team[i];
            team[i] = nullptr;
        }
        delete [] team;
        team = nullptr;
    };
    void addPlayer(const string &name);
    string getTeamName();
    FootballPlayer *getPlayer(const string &name) const;
    FootballPlayer *getPlayer(int order) const;
    FootballPlayer *getMVP();
    int getTeamint();
};
FootballTeam::FootballTeam(const string &name){
    
    team = new FootballPlayer*[MAX_PLAYERS];
    //cout<<"team set up ft"<<"\t";     
    if (team == nullptr) {  //checks for memory allocation errors
        cout << "Error: could not allocate memory" << endl;  //prints error message
    }

    teamname = name; //initializes team name
    teamint = 0; //initialize player counter
};
int FootballTeam::getTeamint(){
    //cout<<"testteamint"<<"\t";
    return teamint;
};
string FootballTeam::getTeamName(){
    //cout<<"teamname"<<"\t";
    return teamname;
};
void FootballTeam::addPlayer(const string &name){
    //cout<<"addplayer ft"<<"\t";
    if(teamint < MAX_PLAYERS){
        *(team+teamint) = new FootballPlayer(name);  //creates new player in list of players
        teamint++;  //adds player counter
        //cout<<"playeradded ft"<<"\t";
    }
    else{
        cout<<"Too Many Players! "<<endl;
        return;
    }
};

FootballPlayer *FootballTeam::getPlayer(const string &name) const{
    //cout<<"searching for player"<<endl;
    for(int i = 0; i<teamint; i++){  //iterates through team players
        if((*(team+i))->getName()==name){   //uses class method to return player name and compare to parameter name
            //cout<<"player retrieved"<<"\t";
            return (*(team+i));  //returns the class FootballPlayer
        }
    }
    cout<<"Player "<< name << " was not found."<<endl;  //output statement upon failure to find player
    return nullptr;
};
FootballPlayer *FootballTeam::getPlayer(int order) const{
    if(order<teamint){
        return *(team+order);  //returns the class FootballPlayer
    }  
    return nullptr;
};
FootballPlayer *FootballTeam::getMVP(){
    int max = -1;  //mvp index to nonexistent (default)
    for(int i = 0; i<teamint; i++){  //iterates known team
        if(i == 0){
            max = i; //records mvp location in team array
        }
        else if((*(team+max))->getPointsScored()<(*(team+i))->getPointsScored()){  //checks if max recorded score is less than other teamates
            max = i;  //reassigns mvp to highest scorer
        }
    }
    if(max==-1||(*(team+max))->getPointsScored()==0){
        return nullptr;   //no points scored or no players
    }
    else{
        return *(team+max); //returns mvp
    }
    
}
// TODO: FootballTeam methods


struct FootballTeamStats {
    int score = 0;
    string mvpName;
    int mvpScore;
};

struct FootballGameStats {
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};

class FootballGame {
private:
    // TODO
    FootballGameStats *gameboard; //overall record of game information

    FootballTeam *home;   //home team class
    FootballTeam *away;   //away team class

public:
    FootballGame(const string &homeName, const string &visitorsName);
    ~FootballGame();
    void addPlayer(Side side, const string &name);  //side 0 = home  ||  side 1 = visitor
    void recordScore(Side side, const string& playerName, int points);
    void assignMVP(Side side);
    FootballGameStats getStats() const;
};
FootballGame::FootballGame(const string &homeName, const string &visitorsName){
    home = new FootballTeam(homeName);
    //cout<<"created home fg"<<"\t";
    away = new FootballTeam(visitorsName);
    //cout<<"created away fg"<<"\t";
    gameboard = new FootballGameStats;
};
void FootballGame::addPlayer(Side side, const string &name){
    //cout<<"add player fg"<<"\t";
    switch (side)
    {
    case HOME:
        home->addPlayer(name);
        break;
    
    case VISITORS:
        away->addPlayer(name);
        break;
    }
};
void FootballGame::recordScore(Side side, const string& playerName, int points){
    //cout<<"score recording"<<"\t";
    if((points<7&&points>0)&&points!=4&&points!=5){   //only valid scoring 1, 2, 3, 6
        //cout<<"entered if statement"<<side<<"\t";
        switch (side)//switch case for which team gets the points and which player gets them.
        {
        case HOME:
            home->getPlayer(playerName)->recordScore(points); //records player points for home team
            gameboard->homeStats.score+=points; //keeps score points home
            //cout<<"home stat"<<gameboard->homeStats.score;
            break;
        
        case VISITORS:
            away->getPlayer(playerName)->recordScore(points); //records player points for away team
            gameboard->visitorsStats.score+=points; //keeps score points away
            //cout<<"away stat"<<gameboard->visitorsStats.score;
            break;
        }
    }else{
        cout<<"Invalid Points!"<<endl;
        return;
    }
    
};
void FootballGame::assignMVP(Side side){
    FootballPlayer *tempPlayer;
    switch (side)//switch case teams
        {
    case HOME:
        tempPlayer = home->getMVP();  //mvp player of team
        gameboard->homeStats.mvpName = tempPlayer->getName();
        gameboard->homeStats.mvpScore = tempPlayer->getPointsScored();
        break;
    
    case VISITORS:
        tempPlayer = away->getMVP();  //mvp player of team
        gameboard->visitorsStats.mvpName = tempPlayer->getName();
        gameboard->visitorsStats.mvpScore = tempPlayer->getPointsScored();
        break;
    }
    delete tempPlayer;
    tempPlayer = nullptr;
};

FootballGameStats FootballGame::getStats() const{
    if((gameboard->homeStats.score)>(gameboard->visitorsStats.score)){
        gameboard->winner = HOME;
    }
    else{
        gameboard->winner = VISITORS;
    }
    return *gameboard;
};

// TODO: FootballGame methods


class FootballFileParser {
private:
    ifstream ifs;
    string filename;
    int lines;
    FootballGameStats gameReport; //actually printed

public:
    FootballFileParser(const string &filepath);
    ~FootballFileParser(){
        ifs.close();
    }
    bool read();  // Returns false if parsing fails
    void printStats() const;
    void readlines();
};

FootballFileParser::FootballFileParser(const string &filepath){
    filename = filepath;
    ifs.open(filepath);
};

void FootballFileParser::printStats() const{
    cout<<"Home: " << gameReport.homeStats.score<<endl;
    cout<<"Visitors: " << gameReport.visitorsStats.score<<endl;
    if(gameReport.winner==0){
        cout<<"Winner: Home"<<endl;
    } else if(gameReport.winner==1){
        cout<<"Winner: Visitors"<<endl;
    }
    
    cout<<"Highest scorers: "<<endl;
    cout<<"\tHome: " << gameReport.homeStats.mvpName << " ("<<gameReport.homeStats.mvpScore<<")" <<endl;
    cout<<"\tVisitors: " << gameReport.visitorsStats.mvpName << " ("<<gameReport.visitorsStats.mvpScore<<")" <<endl;
};

void FootballFileParser::readlines(){
    ifstream run1;
    run1.open("filename");
    string filler;
    
    while(getline(run1, filler)){
        lines++;
    }
    run1.close();
}

bool FootballFileParser::read(){
    if(!ifs){
        cout<<"error opening file"<<endl;
        return false;
    }
    string stringcont, playernames, tally;
    stringstream ss, ff;
    getline(ifs, stringcont);
    //const string *name1 = &stringcont;
    
    FootballTeam *start = new FootballTeam(stringcont);//*name1
    if(start==nullptr){
        cout<<"pointer error";
    }
    getline(ifs, stringcont);
    ss<<stringcont;
    while(getline(ss, playernames, ',')){
        start->addPlayer(playernames);
    }
    getline(ifs, stringcont);
    //const string *name1 = &stringcont;

    FootballTeam *seco = new FootballTeam(stringcont);//*name2
    if(seco==nullptr){
        cout<<"pointer error";
    }
    getline(ifs, stringcont);
    ff<<stringcont;
    while(getline(ff, playernames, ',')){
        seco->addPlayer(playernames);
    }

    //create football game and replace pointers
    FootballGame *report = new FootballGame(start->getTeamName(), seco->getTeamName());
    if(report==nullptr)
        cout<<"pointer error";

    for(int i = 0; i<start->getTeamint(); i++){
        report->addPlayer(HOME, (start->getPlayer(i)->getName()));  //first team
    }
    for(int i = 0; i<seco->getTeamint(); i++){
        report->addPlayer(VISITORS, (seco->getPlayer(i)->getName()));   //second team
    }

    delete start, seco;
    start = nullptr;
    seco = nullptr;
    

    //game start
    Side VH;
    string scorer;
    string point;
    int scorewon;

    
    while(getline(ifs, stringcont)){
        stringstream tt;
        tt<<stringcont;
        int idx = 0;
        while(idx<3){
            getline(tt, stringcont, ',');
            switch(idx){
                case 0:
                    if(stringcont=="V")
                    {
                        VH = VISITORS;
                    }
                    else VH = HOME;
                    break;
                case 1:
                    scorer = stringcont;
                    break;
                case 2:
                    point = stringcont;
                    break;
            }
            idx++;
        }
        if(point=="TD"){
            //cout<<"touchdown"<<"\t";
            scorewon = 6;
        }else if(point=="FG"){
            //cout<<"fieldgoal"<<"\t";
            scorewon = 3;

        }else if(point=="S"){
            //cout<<"safety"<<"\t";
            scorewon = 2;

        }else if(point=="EP"){
            //cout<<"extra point"<<"\t";
            scorewon = 1;
        }

        report->recordScore(VH, scorer, scorewon);  //reports score won by player
    }
    report->assignMVP(HOME);
    report->assignMVP(VISITORS);
    report->getStats();
    gameReport = report->getStats();

/*
    game
    void addPlayer(Side side, const string &name);  //side 0 = home  ||  side 1 = visitor
    void recordScore(Side side, const string& playerName, int points);
    void assignMVP(Side side);
    FootballGameStats getStats() const;

    team
    void addPlayer(const string &name);
    string getTeamName();
    FootballPlayer *getPlayer(const string &name) const;
    FootballPlayer *getPlayer(int order) const;
    FootballPlayer *getMVP();
    int getTeamint();
*/
    return true;

};

// TODO: FootballFileParser methods


int main(int argc, char **argv)
{
    string filename = "superbowl.csv";
    FootballFileParser *superBowl = new FootballFileParser(filename);
    //cout<<"parser defined m"<<"\t";
    if(superBowl==nullptr){
        cout<<"Pointer Error Occurred";
        return 1;
    }
    //cout<<"commence file read m"<<"\t";
    superBowl->read();
    //cout<<"completed file read m"<<"\t";
    superBowl->printStats();
    delete superBowl;
    superBowl = nullptr;
    return 0;
    // TODO
}
