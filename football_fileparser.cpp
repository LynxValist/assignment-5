
#include "football_fileparser.h"

#include <iostream>
#include <sstream>
#include <iomanip>

FootballFileParser::FootballFileParser(const string &filepath){
    filename = filepath;
    ifs.open(filepath);
};

void FootballFileParser::printStats() const{
    cout<<"Home: " << gameReport.homeStats.score<<endl;
    cout<<"Visitors: " << gameReport.visitorsStats.score<<endl;
    if(gameReport.winner==0){
        cout<<"Winner: Home"<<endl;
    } else if(gameReport.winner==1){
        cout<<"Winner: Visitors"<<endl;
    }
    
    cout<<"Highest scorers: "<<endl;
    cout<<"\tHome: " << gameReport.homeStats.mvpName << " ("<<gameReport.homeStats.mvpScore<<")" <<endl;
    cout<<"\tVisitors: " << gameReport.visitorsStats.mvpName << " ("<<gameReport.visitorsStats.mvpScore<<")" <<endl;
};

void FootballFileParser::readlines(){
    ifstream run1;
    run1.open("filename");
    string filler;
    
    while(getline(run1, filler)){
        lines++;
    }
    run1.close();
}

bool FootballFileParser::read(){
    if(!ifs){
        cout<<"error opening file"<<endl;
        return false;
    }
    string stringcont, playernames, tally;
    stringstream ss, ff;
    getline(ifs, stringcont);
    //const string *name1 = &stringcont;
    
    FootballTeam *start = new FootballTeam(stringcont);//*name1
    if(start==nullptr){
        cout<<"pointer error";
    }
    getline(ifs, stringcont);
    ss<<stringcont;
    while(getline(ss, playernames, ',')){
        start->addPlayer(playernames);
    }
    getline(ifs, stringcont);
    //const string *name1 = &stringcont;

    FootballTeam *seco = new FootballTeam(stringcont);//*name2
    if(seco==nullptr){
        cout<<"pointer error";
    }
    getline(ifs, stringcont);
    ff<<stringcont;
    while(getline(ff, playernames, ',')){
        seco->addPlayer(playernames);
    }

    //create football game and replace pointers
    FootballGame *report = new FootballGame(start->getTeamName(), seco->getTeamName());
    if(report==nullptr)
        cout<<"pointer error";

    for(int i = 0; i<start->getTeamint(); i++){
        report->addPlayer(HOME, (start->getPlayer(i)->getName()));  //first team
    }
    for(int i = 0; i<seco->getTeamint(); i++){
        report->addPlayer(VISITORS, (seco->getPlayer(i)->getName()));   //second team
    }

    delete start, seco;
    start = nullptr;
    seco = nullptr;
    

    //game start
    Side VH;
    string scorer;
    string point;
    int scorewon;

    
    while(getline(ifs, stringcont)){
        stringstream tt;
        tt<<stringcont;
        int idx = 0;
        while(idx<3){
            getline(tt, stringcont, ',');
            switch(idx){
                case 0:
                    if(stringcont=="V")
                    {
                        VH = VISITORS;
                    }
                    else VH = HOME;
                    break;
                case 1:
                    scorer = stringcont;
                    break;
                case 2:
                    point = stringcont;
                    break;
            }
            idx++;
        }
        if(point=="TD"){
            //cout<<"touchdown"<<"\t";
            scorewon = 6;
        }else if(point=="FG"){
            //cout<<"fieldgoal"<<"\t";
            scorewon = 3;

        }else if(point=="S"){
            //cout<<"safety"<<"\t";
            scorewon = 2;

        }else if(point=="EP"){
            //cout<<"extra point"<<"\t";
            scorewon = 1;
        }

        report->recordScore(VH, scorer, scorewon);  //reports score won by player
    }
    report->assignMVP(HOME);
    report->assignMVP(VISITORS);
    report->getStats();
    gameReport = report->getStats();
    return true;

};