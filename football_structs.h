#ifndef INITIALIZE
#define INITIALIZE

#include <cstdlib>
#include <string>
using namespace std;

enum Side { HOME, VISITORS };

const int MAX_PLAYERS = 100;

struct FootballTeamStats {
    int score = 0;
    string mvpName;
    int mvpScore;
};

struct FootballGameStats {
    Side winner;
    FootballTeamStats homeStats;
    FootballTeamStats visitorsStats;
};

#endif