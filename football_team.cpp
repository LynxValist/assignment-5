#include "football_team.h"

#include <iostream>

FootballTeam::FootballTeam(const string &name){
    
    team = new FootballPlayer*[MAX_PLAYERS];
    //cout<<"team set up ft"<<"\t";     
    if (team == nullptr) {  //checks for memory allocation errors
        cout << "Error: could not allocate memory" << endl;  //prints error message
    }

    teamname = name; //initializes team name
    teamint = 0; //initialize player counter
};
int FootballTeam::getTeamint(){
    //cout<<"testteamint"<<"\t";
    return teamint;
};
string FootballTeam::getTeamName(){
    //cout<<"teamname"<<"\t";
    return teamname;
};
void FootballTeam::addPlayer(const string &name){
    //cout<<"addplayer ft"<<"\t";
    if(teamint < MAX_PLAYERS){
        *(team+teamint) = new FootballPlayer(name);  //creates new player in list of players
        teamint++;  //adds player counter
        //cout<<"playeradded ft"<<"\t";
    }
    else{
        cout<<"Too Many Players! "<<endl;
        return;
    }
};

FootballPlayer *FootballTeam::getPlayer(const string &name) const{
    //cout<<"searching for player"<<endl;
    for(int i = 0; i<teamint; i++){  //iterates through team players
        if((*(team+i))->getName()==name){   //uses class method to return player name and compare to parameter name
            //cout<<"player retrieved"<<"\t";
            return (*(team+i));  //returns the class FootballPlayer
        }
    }
    cout<<"Player "<< name << " was not found."<<endl;  //output statement upon failure to find player
    return nullptr;
};
FootballPlayer *FootballTeam::getPlayer(int order) const{
    if(order<teamint){
        return *(team+order);  //returns the class FootballPlayer
    }  
    return nullptr;
};
FootballPlayer *FootballTeam::getMVP(){
    int max = -1;  //mvp index to nonexistent (default)
    for(int i = 0; i<teamint; i++){  //iterates known team
        if(i == 0){
            max = i; //records mvp location in team array
        }
        else if((*(team+max))->getPointsScored()<(*(team+i))->getPointsScored()){  //checks if max recorded score is less than other teamates
            max = i;  //reassigns mvp to highest scorer
        }
    }
    if(max==-1||(*(team+max))->getPointsScored()==0){
        return nullptr;   //no points scored or no players
    }
    else{
        return *(team+max); //returns mvp
    }
    
}
