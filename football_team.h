#ifndef FOOTBALL_TEAM
#define FOOTBALL_TEAM

#include "football_player.h"

class FootballTeam {
private:
    // TODO
    FootballPlayer **team; //list of players
    string teamname;  //team name
    int teamint; //player counter

public:
    FootballTeam(const string &name);
    ~FootballTeam(){
        for(int i = 0; i<teamint; i++){
            delete team[i];
            team[i] = nullptr;
        }
        delete [] team;
        team = nullptr;
    };
    void addPlayer(const string &name);
    string getTeamName();
    FootballPlayer *getPlayer(const string &name) const;
    FootballPlayer *getPlayer(int order) const;
    FootballPlayer *getMVP();
    int getTeamint();
};

#endif
