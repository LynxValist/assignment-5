
#ifndef FOOTBALL_GAME
#define FOOTBALL_GAME

#include "football_team.h"

class FootballGame {
private:
    // TODO
    FootballGameStats *gameboard; //overall record of game information

    FootballTeam *home;   //home team class
    FootballTeam *away;   //away team class

public:
    FootballGame(const string &homeName, const string &visitorsName);
    ~FootballGame();
    void addPlayer(Side side, const string &name);  //side 0 = home  ||  side 1 = visitor
    void recordScore(Side side, const string& playerName, int points);
    void assignMVP(Side side);
    FootballGameStats getStats() const;
};

#endif