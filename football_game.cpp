
#include "football_game.h"
#include <iostream>

using namespace std;


FootballGame::FootballGame(const string &homeName, const string &visitorsName){
    home = new FootballTeam(homeName);
    //cout<<"created home fg"<<"\t";
    away = new FootballTeam(visitorsName);
    //cout<<"created away fg"<<"\t";
    gameboard = new FootballGameStats;
};
void FootballGame::addPlayer(Side side, const string &name){
    //cout<<"add player fg"<<"\t";
    switch (side)
    {
    case HOME:
        home->addPlayer(name);
        break;
    
    case VISITORS:
        away->addPlayer(name);
        break;
    }
};
void FootballGame::recordScore(Side side, const string& playerName, int points){
    //cout<<"score recording"<<"\t";
    if((points<7&&points>0)&&points!=4&&points!=5){   //only valid scoring 1, 2, 3, 6
        //cout<<"entered if statement"<<side<<"\t";
        switch (side)//switch case for which team gets the points and which player gets them.
        {
        case HOME:
            home->getPlayer(playerName)->recordScore(points); //records player points for home team
            gameboard->homeStats.score+=points; //keeps score points home
            //cout<<"home stat"<<gameboard->homeStats.score;
            break;
        
        case VISITORS:
            away->getPlayer(playerName)->recordScore(points); //records player points for away team
            gameboard->visitorsStats.score+=points; //keeps score points away
            //cout<<"away stat"<<gameboard->visitorsStats.score;
            break;
        }
    }else{
        cout<<"Invalid Points!"<<endl;
        return;
    }
    
};
void FootballGame::assignMVP(Side side){
    FootballPlayer *tempPlayer;
    switch (side)//switch case teams
        {
    case HOME:
        tempPlayer = home->getMVP();  //mvp player of team
        gameboard->homeStats.mvpName = tempPlayer->getName();
        gameboard->homeStats.mvpScore = tempPlayer->getPointsScored();
        break;
    
    case VISITORS:
        tempPlayer = away->getMVP();  //mvp player of team
        gameboard->visitorsStats.mvpName = tempPlayer->getName();
        gameboard->visitorsStats.mvpScore = tempPlayer->getPointsScored();
        break;
    }
    delete tempPlayer;
    tempPlayer = nullptr;
};

FootballGameStats FootballGame::getStats() const{
    if((gameboard->homeStats.score)>(gameboard->visitorsStats.score)){
        gameboard->winner = HOME;
    }
    else{
        gameboard->winner = VISITORS;
    }
    return *gameboard;
};