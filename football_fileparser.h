
#ifndef FOOTBALL_FILE
#define FOOTBALL_FILE

#include "football_game.h"
#include <fstream>

class FootballFileParser {
private:
    ifstream ifs;
    string filename;
    int lines;
    FootballGameStats gameReport; //actually printed

public:
    FootballFileParser(const string &filepath);
    ~FootballFileParser(){
        ifs.close();
    }
    bool read();  // Returns false if parsing fails
    void printStats() const;
    void readlines();
};

#endif