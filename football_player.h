//Header for Football Player Class

#ifndef FOOTBALL_PLAYER
#define FOOTBALL_PLAYER

#include "football_structs.h"

class FootballPlayer 
{
    private:
        // TODO
        string playername;
        int playerscore;
        
    public:
        FootballPlayer(const string &name);
        const string &getName() const;
        int getPointsScored() const;
        void recordScore(int points);
};
#endif

//Header