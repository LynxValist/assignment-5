// Implementation file for the FootballPlayer class. 
#include "football_player.h"

#include <iostream>

FootballPlayer::FootballPlayer(const string &name){
    playername = name;
    playerscore = 0;
    //cout<<"created player fp"<<"\t";
};
const string &FootballPlayer::getName() const{
    //cout<<"getname";
    return playername;
};
int FootballPlayer::getPointsScored() const{
    //cout<<"getpoint";
    return playerscore;
};
void FootballPlayer::recordScore(int points){
    //cout<<"record";
    if((points<7&&points>0)&&points!=4&&points!=5) //only valid scoring 1, 2, 3, 6
        playerscore+=points;
};

//