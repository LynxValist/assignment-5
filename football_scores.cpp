#include "football_fileparser.cpp"
#include "football_game.cpp"
#include "football_team.cpp"
#include "football_player.cpp"

#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
    string filename = "superbowl.csv";
    FootballFileParser *superBowl = new FootballFileParser(filename);
    //cout<<"parser defined m"<<"\t";
    if(superBowl==nullptr){
        cout<<"Pointer Error Occurred";
        return 1;
    }
    //cout<<"commence file read m"<<"\t";
    superBowl->read();
    //cout<<"completed file read m"<<"\t";
    superBowl->printStats();
    delete superBowl;
    superBowl = nullptr;
    return 0;
    // TODO
}
